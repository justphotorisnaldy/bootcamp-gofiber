package main

import (
	"coding/app"
	"fmt"
	"log"
)

func main() {
	log.Println(app.Multiply(7.8, 2))

	value1 := 42
	result1, err1 := app.IsInteger(value1)
	fmt.Println(result1, err1)

	value2 := "Hello"
	result2, err2 := app.IsInteger(value2)
	fmt.Println(result2, err2)

	fmt.Println(app.IntToDayName(1)) // Output: senin
	fmt.Println(app.IntToDayName(3)) // Output: selasa
	fmt.Println(app.IntToDayName(7)) // Output: minggu
	fmt.Println(app.IntToDayName(9)) // Output:

	fmt.Println(app.DayNameToInt("senin"))  // Output: 1
	fmt.Println(app.DayNameToInt("selasa")) // Output: 2
	fmt.Println(app.DayNameToInt("minggu")) // Output: 7
	fmt.Println(app.DayNameToInt("kamis"))  // Output: 0

	var kosong string
	app.IsiHello(&kosong)
	fmt.Println(kosong) // Output: hello world
	var nonkosong = "existing value"
	app.IsiHello(&nonkosong)
	log.Println(nonkosong) // Output: existing value
}
